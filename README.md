# Auto Classifier based on LLMs

# Install & Run
What you need:
- A reasonably powerful computer, with a GPU
- A model you can run in the `.gguf` format

**First of all**, edit the `srcs/categorize.py` and edit the `MODEL_PATH` constant.
Then run :
```bash
pipenv install
pipenv shell
export CMAKE_ARGS="-DLLAMA_CUBLAS=on"
export FORCE_CMAKE=1
pip install --upgrade --force-reinstall llama-cpp-python --no-cache-dir
pipenv run python srcs/categorize.py
```