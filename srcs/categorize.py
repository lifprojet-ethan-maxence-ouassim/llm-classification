from models import *
from mongoengine import *
from dataset_extrapolation import generate_prompt_entry
import re
from prompts import *
from llama_cpp import Llama
import logging
# import ollama

MODEL_PATH = "/home/ethan/Téléchargements/openhermes-2.5-mistral-7b.Q5_K_M.gguf"

llm = Llama(MODEL_PATH, n_gpu_layers=29, n_ctx=5000, n_threads=13)

def convert_to_text(document : DataPoint) -> str:
    if isinstance(document, Product):
        t = f"Product {document.name} ; "
    elif isinstance(document, Event):
        t = f"Event {document.name} ; "
    elif isinstance(document, Place):
        t = f"Place {document.name} ; "
    else:
        raise Exception("Unknown type")
    t += f"De catégories : {document.types} ; "
    if document.description is not None and document.description != "":
        desc = str(document.description).replace('\r\n', ' ')
        desc = desc.replace('\n', ' ')
        t += f"Description : {desc} ; "
    for d in document.dates:
        d : TimeInfos
        t += f"Du {d.validFrom} au {d.validThrough} ; " if "validFrom" in d and "validThrough" in d else ""
        t += f"Ouvert de {d.opens} à {d.closes} ; " if "opens" in d and "closes" in d else ""
        t += f"Le {d.dayOfWeek} ; " if "dayOfWeek" in d else ""
    if document.location is not None:
        t += f"Localisé à {document.location.address} ; "
    if document.contact_infos is not None:
        t += f"Contact : {document.contact_infos.phone} ; {document.contact_infos.email} ; "
    if document.review is not None:
        t += f"Note : {document.review.rating} ; "
    if isinstance(document, TimeBoundPoint) and document.prices is not None:
        t += f"Prix : {document.prices.min_price} - {document.prices.max_price} {document.prices.currency} ; "
    return t.replace("#", "HASHTAG")

def make_a_prediction(prompt : str, regex_guard_or_cat : str | list[str]) -> str:
    global llm
    nb_tries = 0
    while nb_tries < 5:
        try:
            category_answer = llm(prompt, temperature=0.3, echo=False)
            category_predicted = category_answer["choices"][0]["text"].rstrip().lstrip()
            # category_answer = ollama.generate(model="mistral", prompt=prompt, temperature=0.3)
            # category_predicted = category_answer["response"].rstrip().lstrip()
        except ValueError:
            category = "IDK"
            break

        if isinstance(regex_guard_or_cat, list):
            categories = regex_guard_or_cat
            for category in categories + ["IDK"]:
                if re.search(category, category_predicted, re.IGNORECASE):
                    return category
        else:
            match = re.search(regex_guard_or_cat, category_predicted, re.IGNORECASE)
            if match is not None:
                return match.group(0)
        nb_tries += 1
    if nb_tries >= 5:
        logging.warning(f"Could not find a category for the text, {category_answer}")
        category = "IDK"
    return category

def extract_tag_from_types(types : list[str]) -> SimplfiedTagsEnum:
    if "schema:Accomodation" in types:
        return SimplfiedTagsEnum.HOTEL
    if "schema:Restaurant" in types:
        return SimplfiedTagsEnum.RESTAURANT
    if "schema:SportsAndLeisurePlace" in types:
        return SimplfiedTagsEnum.SPORT
    if "CulturalSite" in types:
        return SimplfiedTagsEnum.HISTORICAl_LANDMARK
    if "schema:Museum" in types:
        return SimplfiedTagsEnum.MUSEUM
    if "PlayArea" in types:
        return SimplfiedTagsEnum.KIDS_PLAYGROUND
    if "CulturalEvent" in types:
        return SimplfiedTagsEnum.PLAYS_THEATER
    return None

connect(host="mongodb+srv://Ethan:ethan123@cluster0.6iktivc.mongodb.net/MA_BASE_DE_DONNEES")
while DataPoint.objects(criterias__exists=False).count() > 0:
    disconnect()
    connect(host="mongodb+srv://Ethan:ethan123@cluster0.6iktivc.mongodb.net/MA_BASE_DE_DONNEES")
    try:
        for datapoint in DataPoint.objects(criterias__exists=False):
            datapoint : DataPoint
            if datapoint.criterias is not None:
                continue
            prompt_entry : str = generate_prompt_entry(id=15, text=convert_to_text(datapoint), category="")
            
            crits = Criterias()
            age_cat = make_a_prediction(AGE_PROMPT + prompt_entry, [member.value for member in AgeEnum])
            if age_cat == "IDK":
                age_cat = "baby"
            crits.age_category = AgeEnum(age_cat)

            estim_duration = make_a_prediction(ESTIMATED_DURATION_PROMPT + prompt_entry, r"\d+")
            if estim_duration == "IDK":
                estim_duration = "90"
            crits.estimated_duration = int(estim_duration)

            simplified_tag = extract_tag_from_types(datapoint.types)
            if simplified_tag is not None:
                crits.simplified_tag = simplified_tag
            
            # toursitic_interest = make_a_prediction(TOURISTIC_INTEREST_PROMPT + prompt_entry, ["Oui", "Non"])
            # crits.touristic_interest = bool(toursitic_interest == "Oui") # Just to clearly show the cast

            numerical_interest = make_a_prediction(NUMERICAL_INTEREST_PROMPT + prompt_entry, r"\d+|IDK")
            crits.numerical_interest = int(numerical_interest) / 100

            datapoint.criterias = crits
            try:
                datapoint.save()
            except:
                logging.warning(f"Could not save the document {datapoint.name}")
                continue
    except:
        logging.warning("Could not connect to the database")
        continue
