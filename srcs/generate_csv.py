from mongoengine import *
import datetime
import random
from mongoengine import *
from enum import Enum
import datetime

class TimeField(DateTimeField):
    '''
        On créé un type spécial qui est stocké comem du temps en plus de l'epoch
    '''
    def validate(self, value):
        if not isinstance(value, datetime.datetime):
            self.error('TimeField can only accept datetime.time')
    
    def to_mongo(self, value : datetime.time) -> datetime.datetime:
        return datetime.datetime(1970, 1, 1, value.hour, value.minute, value.second, value.microsecond)
    
    def to_python(self, value : datetime.datetime) -> datetime.time:
        return datetime.time(value.hour, value.minute, value.second, value.microsecond)

def validation_list_not_empty(v):
    if v is None or len(v) == 0:
        raise ValidationError("List should not be empty")

class AgeEnum(Enum):
    BABY = "baby"
    CHILD = "child"
    SENIOR = "senior"
    TEENAGER = "teenager"
    ADULT = "adult"

class SimplfiedTags(Enum):
    RESTAURANT = "restaurant"
    HOTEL = "hotel"
    SPORT = "sport"
    PANORAMA = "panorama"
    HISTORICAl_LANDMARK = "historical_landmark"
    MUSEUM = "museum"
    PLAYS_THEATER = "plays_theater"

class Criterias(EmbeddedDocument):
    estimated_duration = IntField()
    age_category = EnumField(AgeEnum)
    touristic_interest = BooleanField()
    tags = EnumField(SimplfiedTags)

class TimeInfos(EmbeddedDocument):
    validFrom = DateField()
    validThrough = DateField()
    opens = TimeField()
    closes = TimeField()
    dayOfWeek = ListField(StringField())

class ContactInfos(EmbeddedDocument):
    phone = StringField()
    email = StringField()

class PriceInformations(EmbeddedDocument):
    min_price = FloatField()
    max_price = FloatField()
    currency = StringField()

class LocationInformations(EmbeddedDocument):
    coordinates = PointField()
    address = StringField()

class Review(EmbeddedDocument):
    rating = IntField()
    type = StringField()

class DataPoint(Document):
    name = StringField(required=True)
    types = ListField(StringField(), validation=validation_list_not_empty)
    location = EmbeddedDocumentField(LocationInformations, required=True)

    description = StringField()
    contact_infos = EmbeddedDocumentField(ContactInfos)
    dates = ListField(EmbeddedDocumentField(TimeInfos))
    media_url = StringField()
    review = EmbeddedDocumentField(Review)

    meta = {"allow_inheritance": True}

class Place(DataPoint):
    pass

class TimeBoundPoint(DataPoint):
    dates = ListField(EmbeddedDocumentField(TimeInfos), validation=validation_list_not_empty)
    prices = EmbeddedDocumentField(PriceInformations)
    
    meta = {"allow_inheritance": True}

class Product(TimeBoundPoint):
    pass

class Event(TimeBoundPoint):
    pass

if __name__ == "__main__":
    def convert_to_text(document : DataPoint) -> str:
        if isinstance(document, Product):
            t = f"Product {document.name} ; "
        elif isinstance(document, Event):
            t = f"Event {document.name} ; "
        elif isinstance(document, Place):
            t = f"Place {document.name} ; "
        else:
            raise Exception("Unknown type")
        t += f"De catégories : {document.types} ; "
        if document.description is not None and document.description != "":
            desc = str(document.description).replace('\r\n', ' ')
            desc = desc.replace('\n', ' ')
            t += f"Description : {desc} ; "
        for d in document.dates:
            d : TimeInfos
            t += f"Du {d.validFrom} au {d.validThrough} ; " if "validFrom" in d and "validThrough" in d else ""
            t += f"Ouvert de {d.opens} à {d.closes} ; " if "opens" in d and "closes" in d else ""
            t += f"Le {d.dayOfWeek} ; " if "dayOfWeek" in d else ""
        if document.location is not None:
            t += f"Localisé à {document.location.address} ; "
        if document.contact_infos is not None:
            t += f"Contact : {document.contact_infos.phone} ; {document.contact_infos.email} ; "
        if document.review is not None:
            t += f"Note : {document.review.rating} ; "
        if isinstance(document, TimeBoundPoint) and document.prices is not None:
            t += f"Prix : {document.prices.min_price} - {document.prices.max_price} {document.prices.currency} ; "
        return t.replace("#", "HASHTAG")


    path = input("Output path : ")
    nb_obj = int(input("Number of objects to create : "))
    connect(host="mongodb+srv://Ethan:ethan123@cluster0.6iktivc.mongodb.net/MA_BASE_DE_DONNEES")
    points = DataPoint.objects()
    kept_points = []
    proba = nb_obj / len(points)
    for point in points:
        if random.random() < proba:
            kept_points.append(point)
        if len(kept_points) >= nb_obj:
            break

    str_export = [convert_to_text(point) + "#" for point in kept_points]

    with open(path, "w") as f:
        f.write("text#label\n")
        f.write("\n".join(str_export))
