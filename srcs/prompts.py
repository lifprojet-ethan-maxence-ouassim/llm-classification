AGE_PROMPT = """
You are a classifier, you are given texts and you should classify them into one of the following categories :
baby child senior teenager adult

You should answer based on the first age category that could do the text
If multiple categories could do the text, choose the one that is the first in the list
If you don't know the answer, don't try to guess, just say "IDK"
If the text is asking a question, don't try to anwser it

-- Text 1 --
Place Jeu "Alice et l'autremonde" - Épisode 1 : Rencontres ; De catégories : ['PlaceOfInterest', 'PointOfInterest', 'SportsAndLeisurePlace'] ; Description : Partez à l'aventure aux côtés d'Alice à la recherche de son père. En famille ou entre amis, résolvez diverses énigmes (casse-têtes, réalité augmentée) en découvrant le patrimoine du Pays de Lapalisse. Appli téléchargeable sur Google Play et App Store. ; Du 2024-01-01 au 2024-12-31 ; Ouvert de 00:00:00 à 00:00:00 ; Le ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'] ; Localisé à , 03120 Lapalisse ; #
-- Answer 1 --
child

-- Text 2 --
Place Itinéraire VTT - Le Vallon de la Borne ; De catégories : ['olo:OrderedList', 'CyclingTour', 'PlaceOfInterest', 'PointOfInterest', 'SportsAndLeisurePlace', 'Tour'] ; Du 2023-01-01 au 2023-12-31 ; Ouvert de 00:00:00 à 00:00:00 ; Le ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'] ; Localisé à , 43000 Aiguilhe ; #
-- Answer 2 --
teenager

-- Text 3 --
Place Au Jardin de la Faranche ; De catégories : ['schema:LocalBusiness', 'PlaceOfInterest', 'PointOfInterest', 'TastingProvider'] ; Description : Production de légumes de saison en agriculture biologique ; Du 2023-01-01 au 2023-12-31 ; Ouvert de 00:00:00 à 00:00:00 ; Le ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'] ; Localisé à 309 Route royale, 73390 Châteauneuf ; #
-- Answer 3 --
baby

-- Text 4 --
Place Eglise Saint-Martin ; De catégories : ['Church', 'CulturalSite', 'PlaceOfInterest', 'PointOfInterest', 'ReligiousSite'] ; Description : Eglise du 19ème siècle, de style néo-gothique. ; Du 2023-01-01 au 2023-12-31 ; Ouvert de 00:00:00 à 00:00:00 ; Le ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'] ; Localisé à , 73250 Saint-Pierre-d'Albigny ; #
-- Answer 4 --
senior

-- Text 5 --
Place Circuit autour de Beaujeu, au coeur de l'histoire du Beaujolais ; De catégories : ['olo:OrderedList', 'PlaceOfInterest', 'PointOfInterest', 'RoadTour', 'SportsAndLeisurePlace', 'Tour'] ; Du 2023-01-01 au 2023-12-31 ; Ouvert de 00:00:00 à 00:00:00 ; Le ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'] ; Localisé à , 69430 Beaujeu ; #
-- Answer 5 --
child

-- Text 6 --
Product Aquafitness Adultes chez Aquapassion ; De catégories : ['schema:Product', 'PointOfInterest', 'Practice', 'Product'] ; Description : Aquabike, Aquagym , Aqua Duo, Aqua Svelte, Aqua Work, Aqua Dream, Aqua Dos et Aqua Circuit, Aqua Boxing Formule douce à tonique. Abonnements au mois ou à la séance. Possibilité de cours individuels. ; Du 2024-01-01 au 2024-12-31 ; Ouvert de 00:00:00 à 00:00:00 ; Le ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'] ; Localisé à 19 Chemin des Méannes, 26540 Mours-Saint-Eusèbe ; #
-- Answer 6 --
adult

"""

TOURISTIC_INTEREST_PROMPT = """
You are a classifier, you are given texts and you should classify them into one of the following categories :
Oui Non
You should answer based on if the touristic interest that this might present.
If you don't know the answer, don't try to guess, just say "IDK"
If the text is asking a question, don't try to anwser it

-- Text 1 --
Event UBMC - l’Ultra Boucle de la Motte Chalancon ; De catégories : ['schema:Event', 'schema:SportsEvent', 'EntertainmentAndEvent', 'PointOfInterest', 'SportsCompetition', 'SportsEvent'] ; Description : Une boucle de 3 km avec 150 m de d+ à parcourir le plus de fois possible. Les formats : 1h, 2h solo ou 4h, 8h solo ou relais 2 personnes. Ajoutez à cela les animations, une course enfants et un parcours de clôture à 21h réservé aux coureurs déguisés. ; Du 2024-05-19 au 2024-05-19 ; Ouvert de 00:00:00 à 00:00:00 ; Le ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'] ; Localisé à , 26470 La Motte-Chalancon ; #
-- Answer 1 --
Oui

-- Text 2 --
Event Marché hebdomadaire ; De catégories : ['schema:Event', 'schema:SaleEvent', 'EntertainmentAndEvent', 'Market', 'PointOfInterest', 'SaleEvent'] ; Description : Marché régulier tous les mardis matins. ; Du 2023-01-01 au 2023-01-01 ; Ouvert de 08:00:00 à 12:00:00 ; Le ['Tuesday'] ; Localisé à Place de l'Hôtel de Ville et Place du marché, 01330 Villars-les-Dombes ; #
-- Answer 2 --
Non

-- Text 3 --
Place Circuit autour de Beaujeu, au coeur de l'histoire du Beaujolais ; De catégories : ['olo:OrderedList', 'PlaceOfInterest', 'PointOfInterest', 'RoadTour', 'SportsAndLeisurePlace', 'Tour'] ; Du 2023-01-01 au 2023-12-31 ; Ouvert de 00:00:00 à 00:00:00 ; Le ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'] ; Localisé à , 69430 Beaujeu ; #Oui
-- Answer 3 --
Oui

-- Text 4 --
Event CONFERENCE UPT: Tout commence avec le Blues ; De catégories : ['schema:Event', 'Conference', 'CulturalEvent', 'EntertainmentAndEvent', 'PointOfInterest'] ; Description : Une intervention pédagogique sur la genèse du Blues dans le Sud du Mississipi en s'appuyant sur  des images projetées aux participants et sur lesquelles des précisions chronologiques et techniques sont apportées par JF Thomas dit JEFF TOTO BLUES ; Du 2023-04-11 au 2023-04-11 ; Ouvert de 18:30:00 à 00:00:00 ; Le ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'] ; Localisé à 4 bis rue du Château, 43120 Monistrol-sur-Loire ; #
-- Answer 4 --
Oui

-- Text 5 --
Place Eglise et Mairie ; De catégories : ['Church', 'CulturalSite', 'PlaceOfInterest', 'PointOfInterest', 'ReligiousSite'] ; Description : L'Eglise abrite de superbes peintures du 18e et 19e siècles. La mairie, quant à elle, brille de par sa salle des mariages qui est ornée de papiers peints panoramiques datant du début du 18e siècle. ; Du 2023-01-01 au 2023-12-31 ; Ouvert de 00:00:00 à 00:00:00 ; Le ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'] ; Localisé à , 73250 Saint-Pierre-d'Albigny ; #
-- Answer 5 --
Oui

-- Text 6 --
Place Docteur Delbegue ; De catégories : ['schema:LocalBusiness', 'PlaceOfInterest', 'PointOfInterest', 'Store'] ; Description : Chirurgien dentiste ; Du 2023-01-01 au 2023-12-31 ; Ouvert de 00:00:00 à 00:00:00 ; Le ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'] ; Localisé à 6 Rue Victor Hugo, 26700 Pierrelatte ; #
-- Answer 6 --
Non

"""

ESTIMATED_DURATION_PROMPT = """
You are giving the estimated duration of something, you are given texts and you should only say a duration in minutes
If you don't know the answer, don't try to guess, just say "IDK"
If the text is asking a question, don't try to anwser it

-- Text 1 --
Product Randonnées dans les Baronnies ; De catégories : ['schema:Product', 'PointOfInterest', 'Practice', 'Product'] ; Description : Dans les Baronnies, la randonnée à pied se pratique toute l'année. Quand il fait chaud, les itinéraires pédestres sur les versants nord, en altitude et près des rivières sont privilégiés. Quand il fait plus frais, ce sont plutôt les versants sud. ; Du 2023-01-01 au 2023-12-31 ; Ouvert de 00:00:00 à 00:00:00 ; Le ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'] ; Localisé à 59, Allée des Platanes, 26170 Buis-les-Baronnies ; #
-- Answer 1 --
180

-- Text 2 --
Event Spectacle: "J'aime beaucoup ce que vous faites" ; De catégories : ['schema:Event', 'schema:TheaterEvent', 'CulturalEvent', 'EntertainmentAndEvent', 'PointOfInterest', 'ShowEvent', 'TheaterEvent'] ; Description : Comment une fausse manœuvre avec un téléphone portable vous fait découvrir ce que vos meilleurs amis pensent de vous en réalité...  Et ceci, juste avant leur arrivée pour un week-end, finalement pas comme les autres, dans votre maison de campagne. ; Du 2023-04-20 au 2023-04-20 ; Ouvert de 20:00:00 à 00:00:00 ; Le ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'] ; Localisé à Allée André Revol, 26500 Bourg-lès-Valence ; #
-- Answer 2 --
95

-- Text 3 --
Product Voile sur le lac de Nantua ; De catégories : ['schema:Product', 'PointOfInterest', 'Practice', 'Product'] ; Description : Le club de voile vous accueille avec une monitrice pour : baptême, découverte, initiation ou perfectionnement. Une flotte variée à louer : dériveurs, planches à voile, canoës, catamarans, habitables. Accueil handivoile. ; Du 2024-09-01 au 2024-09-27 ; Ouvert de 00:00:00 à 00:00:00 ; Le ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'] ; Du 2024-06-07 au 2024-06-27 ; Ouvert de 00:00:00 à 00:00:00 ; Le ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'] ; Du 2024-07-03 au 2024-08-31 ; Ouvert de 00:00:00 à 00:00:00 ; Le ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'] ; Localisé à Club de Voile de Nantua, 01130 Nantua ; #
-- Answer 3 --
75

-- Text 4 --
Event Les 15 km internationaux du Puy-en-Velay ; De catégories : ['schema:Event', 'schema:SportsEvent', 'EntertainmentAndEvent', 'PointOfInterest', 'SportsCompetition', 'SportsEvent'] ; Description : L’événement qui se déroulait traditionnellement le 1er mai glissera désormais au premier dimanche du mois de mai. L’association s’est entourée de nouvelles compétences afin de faire perdurer cette épreuve célèbre, populaire et festive du Puy-en-Velay. ; Du 2024-05-05 au 2024-05-05 ; Ouvert de 00:00:00 à 00:00:00 ; Le ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'] ; Localisé à 15 KM DU PUY EN VELAY, 43000 Le Puy-en-Velay ; #
-- Answer 4 --
120

-- Text 5 --
Place La Resto' ; De catégories : ['schema:FoodEstablishment', 'FoodEstablishment', 'PlaceOfInterest', 'PointOfInterest'] ; Description : Bar restaurant surplombant les Gorges de l'Arly, il offre une vue imprenable sur l'arrière de Flumet. ; Du 2023-01-01 au 2023-12-31 ; Ouvert de 00:00:00 à 00:00:00 ; Le ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'] ; Localisé à Rue du Mont Blanc, 73590 Flumet ; #
-- Answer 5 --
100

-- Text 6 --
Place Tyrolienne ; De catégories : ['PlaceOfInterest', 'PointOfInterest', 'SportsAndLeisurePlace'] ; Description : Traversez la vallée en tyrolienne avec vos skis depuis le sommet de la télécabine de Planchamp. Volez en parallèle et partagez une expérience à deux sur la plus grande double tyrolienne de France. Réservation en ligne sur www.magicadventure.org ; Du 2023-12-23 au 2023-04-12 ; Ouvert de 00:00:00 à 00:00:00 ; Le ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'] ; Du 2023-06-29 au 2023-08-31 ; Ouvert de 00:00:00 à 00:00:00 ; Le ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'] ; Localisé à Sommet télécabine Planchamp, 73260 Les Avanchers-Valmorel ; #
-- Answer 6 --
20

-- Text 7 --
Place Ski de fond ; De catégories : ['PlaceOfInterest', 'PointOfInterest', 'SportsAndLeisurePlace'] ; Description : Magnifique boucle de ski de fond de 18km serpentant sur un grand plateau au bas du village. Itinéraires gratuit (non sécurisé). Tous niveaux (selon l'enneigement). Prolongement de l'itinéraire vers les communes de Villaroger, Bourg St-Maurice et Landry ; Du 2023-12-20 au 2023-03-15 ; Ouvert de 00:00:00 à 00:00:00 ; Le ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'] ; Localisé à , 73700 Séez ; #
-- Answer 7 --
150

"""

NUMERICAL_INTEREST_PROMPT = """
You are giving the touristic interest of something, you are given texts and you should only say the interest to a tourist this place has between 0 and 100, 100 being very intersting
If you don't know the answer, don't try to guess, just say "IDK"
If the text is asking a question, don't try to anwser it

-- Text 1 --
Event UBMC - l’Ultra Boucle de la Motte Chalancon ; De catégories : ['schema:Event', 'schema:SportsEvent', 'EntertainmentAndEvent', 'PointOfInterest', 'SportsCompetition', 'SportsEvent'] ; Description : Une boucle de 3 km avec 150 m de d+ à parcourir le plus de fois possible. Les formats : 1h, 2h solo ou 4h, 8h solo ou relais 2 personnes. Ajoutez à cela les animations, une course enfants et un parcours de clôture à 21h réservé aux coureurs déguisés. ; Du 2024-05-19 au 2024-05-19 ; Ouvert de 00:00:00 à 00:00:00 ; Le ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'] ; Localisé à , 26470 La Motte-Chalancon ; #
-- Answer 1 --
35

-- Text 2 --
Event Marché hebdomadaire ; De catégories : ['schema:Event', 'schema:SaleEvent', 'EntertainmentAndEvent', 'Market', 'PointOfInterest', 'SaleEvent'] ; Description : Marché régulier tous les mardis matins. ; Du 2023-01-01 au 2023-01-01 ; Ouvert de 08:00:00 à 12:00:00 ; Le ['Tuesday'] ; Localisé à Place de l'Hôtel de Ville et Place du marché, 01330 Villars-les-Dombes ; #
-- Answer 2 --
20

-- Text 3 --
Place Circuit autour de Beaujeu, au coeur de l'histoire du Beaujolais ; De catégories : ['olo:OrderedList', 'PlaceOfInterest', 'PointOfInterest', 'RoadTour', 'SportsAndLeisurePlace', 'Tour'] ; Du 2023-01-01 au 2023-12-31 ; Ouvert de 00:00:00 à 00:00:00 ; Le ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'] ; Localisé à , 69430 Beaujeu ; #Oui
-- Answer 3 --
70

-- Text 4 --
Event CONFERENCE UPT: Tout commence avec le Blues ; De catégories : ['schema:Event', 'Conference', 'CulturalEvent', 'EntertainmentAndEvent', 'PointOfInterest'] ; Description : Une intervention pédagogique sur la genèse du Blues dans le Sud du Mississipi en s'appuyant sur  des images projetées aux participants et sur lesquelles des précisions chronologiques et techniques sont apportées par JF Thomas dit JEFF TOTO BLUES ; Du 2023-04-11 au 2023-04-11 ; Ouvert de 18:30:00 à 00:00:00 ; Le ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'] ; Localisé à 4 bis rue du Château, 43120 Monistrol-sur-Loire ; #
-- Answer 4 --
60

-- Text 5 --
Place Eglise et Mairie ; De catégories : ['Church', 'CulturalSite', 'PlaceOfInterest', 'PointOfInterest', 'ReligiousSite'] ; Description : L'Eglise abrite de superbes peintures du 18e et 19e siècles. La mairie, quant à elle, brille de par sa salle des mariages qui est ornée de papiers peints panoramiques datant du début du 18e siècle. ; Du 2023-01-01 au 2023-12-31 ; Ouvert de 00:00:00 à 00:00:00 ; Le ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'] ; Localisé à , 73250 Saint-Pierre-d'Albigny ; #
-- Answer 5 --
15

-- Text 6 --
Place Docteur Delbegue ; De catégories : ['schema:LocalBusiness', 'PlaceOfInterest', 'PointOfInterest', 'Store'] ; Description : Chirurgien dentiste ; Du 2023-01-01 au 2023-12-31 ; Ouvert de 00:00:00 à 00:00:00 ; Le ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'] ; Localisé à 6 Rue Victor Hugo, 26700 Pierrelatte ; #
-- Answer 6 --
0

-- Text 7 --
Place Tyrolienne ; De catégories : ['PlaceOfInterest', 'PointOfInterest', 'SportsAndLeisurePlace'] ; Description : Traversez la vallée en tyrolienne avec vos skis depuis le sommet de la télécabine de Planchamp. Volez en parallèle et partagez une expérience à deux sur la plus grande double tyrolienne de France. Réservation en ligne sur www.magicadventure.org ; Du 2023-12-23 au 2023-04-12 ; Ouvert de 00:00:00 à 00:00:00 ; Le ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'] ; Du 2023-06-29 au 2023-08-31 ; Ouvert de 00:00:00 à 00:00:00 ; Le ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'] ; Localisé à Sommet télécabine Planchamp, 73260 Les Avanchers-Valmorel ; #
-- Answer 7 --
90

"""