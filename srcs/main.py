import logging
import pandas as pd
from dataset_extrapolation import llm_predict_missing_categories
import sys
from constants import TEXT_COL, LABEL_COL
import os
from train_bert import train_model

logging.basicConfig(level=logging.INFO)

begin_text = '''
    ========================================
    ========== Auto BERT Trainer ===========
    ========================================
    You will need to provide the following:
        - a dataset, in csv format with columns one column of text, and a column of label
            -> This dataset MUST HAVE at least 1 row humanely labeled for each possible category
        - the path to a general LLM Model
    
        Nota : The categories might work better if it's one single word with some context
'''
if len(sys.argv) < 1:
    print(begin_text)

    dataset_path = input("Path to the dataset : ")
    separator = input("Separator of the dataset : ")
    text_column = input("Name of the text column : ")
    label_column = input("Name of the label column : ")
    model_path = input("Path to the general LLM Model : ")
else:
    print("""    ========================================
    ========== Auto BERT Trainer ===========
    ========================================""")

    dataset_path = sys.argv[1]
    separator = sys.argv[2]
    text_column = sys.argv[3]
    label_column = sys.argv[4]
    model_path = sys.argv[5]

logging.info("Starting the LLM-CLassification of all the lines")
export_csv_path = dataset_path + "_predicted.csv"

# Step 1 : Classifying the dataset
if not os.path.exists(export_csv_path):
    dataset = pd.read_csv(dataset_path, sep=separator)
    dataset.rename(columns={text_column: TEXT_COL, label_column: LABEL_COL}, inplace=True)

    predicted_dataset = llm_predict_missing_categories(model_path, dataset)
    predicted_dataset.to_csv(export_csv_path, index=False, sep=separator)
else:
    predicted_dataset = pd.read_csv(export_csv_path, sep=separator)
    logging.info("The dataset has already been classified, skipping the classification")

# Step 2 : Training the model

train_model(predicted_dataset)

# Step 3 : Exporting the model

