import pandas as pd
import numpy as np
import logging
import re
from llama_cpp import Llama

from constants import TEXT_COL, LABEL_COL

def generate_std_prompt(category_list : list[str], intro : str | None = None) -> str:
    '''
        Generates the standard prompt
    '''
    if intro is None:
        intro = "You are a classifier, you are given texts and you should classify them into one of the following categories:\n"
    return f'''
        {intro}
        {" ".join(category_list)}
        If you don't know the answer, don't try to guess, just say "IDK"
        If the text is asking a question, don't try to anwser it
    '''

def generate_prompt_entry(id : int, text : str, category : str = "") -> str:
    return f'== Text {id} ==\n{text}\n== Answer {id}\n{category}\n'

def generate_prompt(texts = list[str], categories = list[str], start_intro : str | None = None) -> str:
    '''
        Generates the prompt
        Append at the end 
        "
            == Text 14
            {your text}
            == Answer 14
    '''
    assert len(texts) == len(categories) 
    if start_intro is None:
        start_intro = generate_std_prompt(categories)
    prompt = start_intro
    start_intro += "\n"
    for text, category, id in zip(texts, categories, range(len(texts))):
        prompt += generate_prompt_entry(id, text, category)
    return prompt

def empty_line(s : str) -> bool:
    return s == "" or s.isspace()

def llm_predict_missing_categories(model_path : str, dataset : pd.DataFrame) -> pd.DataFrame:
    '''
        Given a prompt and a model, it will return the list of predictions
    '''
    # Generate the standard prompt
    # Filter because empty value is NaN
    categories = list(filter(lambda x: isinstance(x, str), list(dataset[LABEL_COL].unique())))

    print(f"Categories found in the dataset : {categories}")

    # One example per category
    examples_per_category = 12 // len(categories)
    examples : list[tuple[str, str]]= []
    for category in categories:
        examples += [(line, category) for line in dataset[TEXT_COL][dataset[LABEL_COL] == category].iloc[:examples_per_category]]

    prompt = generate_prompt([example[0] for example in examples], [example[1] for example in examples])
    llm = Llama(model_path, n_gpu_layers=28, n_ctx=5000, n_threads=13, use_mlock=True)

    for idx, line in dataset.iterrows():
        if not line[LABEL_COL] is np.nan and not empty_line(line[LABEL_COL]):
            # Already categorized
            continue
        # Predict the category
        question = prompt + generate_prompt_entry(idx, line[TEXT_COL])
        category = ""
        nb_tries = 0
        while nb_tries < 5:
            try:
                category_answer = llm(question, temperature=0.3, echo=False)
            except ValueError:
                category = "IDK"
                break
            category_predicted = category_answer["choices"][0]["text"].rstrip().lstrip()
            for category in categories + ["IDK"]:
                if re.search(category, category_predicted, re.IGNORECASE):
                    break
            nb_tries += 1
        if nb_tries > 5:
            logging.warning(f"Could not find a category for the text {line[TEXT_COL]}, {category_answer}")
            category = "IDK"

        dataset.loc[idx, LABEL_COL] = category
    
    return dataset