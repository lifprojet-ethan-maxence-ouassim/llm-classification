from constants import LABEL_COL, TEXT_COL, TRAIN_FRACTION
from typing import Any
import pandas as pd
from datasets import Dataset, DatasetDict


class Categorizer():
    def __init__(self, categories : list[str]) -> None:
        categories.sort()
        self.cat_to_int = {category: i for i, category in enumerate(categories)}
        self.int_to_cat = {i: category for i, category in enumerate(categories)}
    
    def get_int(self, category : str) -> int:
        return self.cat_to_int.get(category, -1)

    def get_category(self, i : int) -> str:
        return self.int_to_cat.get(i, "IDK")

def train_model(dataset : pd.DataFrame) -> Any:
    categories = list(filter(lambda x: isinstance(x, str), list(dataset[LABEL_COL].unique())))
    categorizer = Categorizer(categories)
    dataset[LABEL_COL] = dataset[LABEL_COL].apply(categorizer.get_int)
    dataset = dataset.sample(frac=1).reset_index(drop=True) # Shuffle the dataset

    train_df = dataset[:int(len(dataset) * TRAIN_FRACTION)]
    test_df = dataset[int(len(dataset) * TRAIN_FRACTION):]
    print(dataset.head())

    # Convert to HuggingFace datasets
    descriptions = DatasetDict({
        "train": Dataset.from_pandas(train_df),
        "test": Dataset.from_pandas(test_df),
    })
    print(descriptions["train"][0])