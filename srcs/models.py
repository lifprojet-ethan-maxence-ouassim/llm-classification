from mongoengine import *
from enum import Enum
import datetime

class TimeField(DateTimeField):
    '''
        On créé un type spécial qui est stocké comem du temps en plus de l'epoch
    '''
    def validate(self, value):
        if value is not None and not isinstance(value, datetime.time):
            self.error('TimeField can only accept datetime.time')
    
    def to_mongo(self, value : datetime.time) -> datetime.datetime:
        if value is None:
            return None
        return datetime.datetime(1970, 1, 1, value.hour, value.minute, value.second, value.microsecond)
    
    def to_python(self, value : datetime.datetime) -> datetime.time:
        if value is None:
            return None
        return datetime.time(value.hour, value.minute, value.second, value.microsecond)

def validation_list_not_empty(v):
    if v is None or len(v) == 0:
        raise ValidationError("List should not be empty")

class AgeEnum(Enum):
    BABY = "baby"
    CHILD = "child"
    SENIOR = "senior"
    TEENAGER = "teenager"
    ADULT = "adult"

class SimplfiedTagsEnum(Enum):
    RESTAURANT = "restaurant"
    HOTEL = "hotel"
    SPORT = "sport"
    PANORAMA = "panorama"
    HISTORICAl_LANDMARK = "historical_landmark"
    MUSEUM = "museum"
    PLAYS_THEATER = "plays_theater"
    KIDS_PLAYGROUND = "kids_playground"

class Criterias(EmbeddedDocument):
    estimated_duration = IntField()
    age_category = EnumField(AgeEnum)
    touristic_interest = BooleanField()
    numerical_interest = FloatField(min_value=0, max_value=1)
    simplified_tag = EnumField(SimplfiedTagsEnum)

class TimeInfos(EmbeddedDocument):
    validFrom = DateField()
    validThrough = DateField()
    opens = TimeField()
    closes = TimeField()
    dayOfWeek = ListField(StringField())

class ContactInfos(EmbeddedDocument):
    phone = StringField()
    email = StringField()

class PriceInformations(EmbeddedDocument):
    min_price = FloatField()
    max_price = FloatField()
    currency = StringField()

class LocationInformations(EmbeddedDocument):
    coordinates = PointField()
    address = StringField()

class Review(EmbeddedDocument):
    rating = IntField()
    type = StringField()

class DataPoint(Document):
    name = StringField(required=True)
    types = ListField(StringField(), validation=validation_list_not_empty)
    location = EmbeddedDocumentField(LocationInformations, required=True)

    description = StringField()
    contact_infos = EmbeddedDocumentField(ContactInfos)
    dates = ListField(EmbeddedDocumentField(TimeInfos))
    media_url = StringField()
    review = EmbeddedDocumentField(Review)

    criterias = EmbeddedDocumentField(Criterias)

    meta = {"allow_inheritance": True}

class Place(DataPoint):
    pass

class TimeBoundPoint(DataPoint):
    dates = ListField(EmbeddedDocumentField(TimeInfos), validation=validation_list_not_empty)
    prices = EmbeddedDocumentField(PriceInformations)
    
    meta = {"allow_inheritance": True}

class Product(TimeBoundPoint):
    pass

class Event(TimeBoundPoint):
    pass